using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    public virtual void triggerInteractable()
    {
        Debug.Log("trigger from parent");
    }

    public virtual void interact()
    {
        Debug.Log("interacting");
    }
}
