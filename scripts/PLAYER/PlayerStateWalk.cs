using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerStateWalk : State
{
    public Player player;

    public PlayerStateWalk(Player _player)
    {
        player = _player;
    }
    public override void Enter()
    {
        player.cc.enabled = true;
        player.cc.moveSpeed = 2.4f;
		player.ws.enabled = true;
    }

    public override void Execute()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            player.sm.ChangeState(player.playerRunState);
        }

		if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            player.sm.ChangeState(player.playerSneakState);
        }
    }
    public override void Exit()
    {
		player.ws.enabled = false;
    }
}
