using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStateDead : State
{
    Player player;
    public PlayerStateDead(Player _player){
        player = _player;
    }

    public override void Enter()
    {
        player.cc.enabled = false;
        player.cc.moveSpeed = 1;
    }

    public override void Execute()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            player.sm.ChangeState(player.playerWalkState);
        }
    }

    public override void Exit()
    {

    }
}
