using System.Collections;
using System.Collections.Generic;
using QFSW.QC;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour
{
	public StateMachine sm;
	public CharController cc;
	public State playerDeadState;
	public State playerWalkState;
	public State playerRunState;
	public State playerSneakState;
	public walkSoundController ws;
	public Scanner scanner;

	void Start()
	{
		playerDeadState = new PlayerStateDead(this);
		playerWalkState = new PlayerStateWalk(this);
		playerRunState = new PlayerStateRun(this);
		playerSneakState = new PlayerStateSneak(this);
		sm = new StateMachine();
		sm.ChangeState(playerWalkState);
	}

	[Command("player.dead")]
	public void switchStateDead()
	{
		sm.ChangeState(playerDeadState);
	}

	[Command("player.idle")]
	public void switchStateIdle()
	{
		sm.ChangeState(playerWalkState);
	}

	void Update()
	{
		sm.Run();
		interact();
	}

	void interact()
	{
		if (scanner.interactableObjectInScanner)
		{
			if (Input.GetKeyDown(KeyCode.E))
			{
				scanner.interactable.interact();
			}
		}
	}
}
