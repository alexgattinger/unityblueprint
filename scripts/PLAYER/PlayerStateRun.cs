using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerStateRun : State
{
    public Player player;

    public PlayerStateRun(Player _player)
    {
        player = _player;
    }
    public override void Enter()
    {
        player.cc.enabled = true;
        player.cc.moveSpeed = 5f;
		AudioManager.i.PlayAtPosition("player_running_constant", player.transform.position, "player_running_constant");
    }

    public override void Execute()
    {
       if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            player.sm.ChangeState(player.playerWalkState);
        }
    }
    public override void Exit()
    {
		AudioManager.i.StopSound("player_running_constant");
    }
}
