using System.Collections;
using System.Collections.Generic;
using QFSW.QC;
using UnityEngine;

public class DoorOpener : Interactable
{
    public Transform doorTransform; // Assign the door's transform here in the inspector
    public float openAngle = 90.0f; // The angle the door should open to
    public float openSpeed = 2.0f; // How fast the door should open
    private bool isOpening = false;
    private bool isClosing = false;
    private Quaternion closedRotation;
    private Quaternion openRotation;
    bool isopen = false;

    void Start()
    {
        closedRotation = doorTransform.rotation;
        openRotation = Quaternion.Euler(doorTransform.eulerAngles + Vector3.up * openAngle);
    }

    void Update()
    {
        if (isOpening)
        {
            isopen = true;
            doorTransform.rotation = Quaternion.Slerp(doorTransform.rotation, openRotation, Time.deltaTime * openSpeed);
            // Check if the door is almost fully open, then stop opening
            if (Quaternion.Angle(doorTransform.rotation, openRotation) < 1.0f)
            {
                doorTransform.rotation = openRotation;
                isOpening = false;
            }
        }
        else if (isClosing)
        {
            isopen = false;
            doorTransform.rotation = Quaternion.Slerp(doorTransform.rotation, closedRotation, Time.deltaTime * openSpeed);
            // Check if the door is almost fully closed, then stop closing
            if (Quaternion.Angle(doorTransform.rotation, closedRotation) < 1.0f)
            {
                doorTransform.rotation = closedRotation;
                isClosing = false;
            }
        }
    }

    // Call this method to open the door
    [Command("open")]
    public void OpenDoor()
    {
        isOpening = true;
        isClosing = false;
    }

    public override void interact()
    {
		Debug.Log("interact with DOOR");
        if (!isopen)
        {
			AudioManager.i.PlayOnceAtPosition("door_open", transform.position);
            isOpening = true;
            isClosing = false;
        }
        else
        {
            isClosing = true;
            isOpening = false;
        }
    }

    [Command("close")]
    // Call this method to close the door
    public void CloseDoor()
    {
        isClosing = true;
        isOpening = false;
    }
}