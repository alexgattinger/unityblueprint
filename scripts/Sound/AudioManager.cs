using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
	public Dictionary<string, AudioClip> sounds = new Dictionary<string, AudioClip>();
	// List<AudioClip> clips = new List<AudioClip>();
	public List<AudioSource> sources = new List<AudioSource>();
	public static AudioManager i;

	public Dictionary<string, AudioSource> soundsByName = new Dictionary<string, AudioSource>();


	// public SoundClip[] soundss;

	/* speichert clips in this.clips */
	// public void registerSounds(List<AudioClip> clips)
	// {
	// 	foreach (var clip in clips)
	// 	{
	// 		sounds.Add(clip.name, clip);
	// 	}
	// }

	public void PlayOnce(string clipName)
	{
		if (checkForClipLoaded(clipName))
		{
			getIdleAudioSource().PlayOneShot(sounds[clipName]);
		}
	}

	public void PlayOnceAtPosition(string clipName, Vector3 pos)
	{
		if (checkForClipLoaded(clipName))
		{
			AudioSource a = getIdleAudioSource();
			a.transform.position = pos;
			a.PlayOneShot(sounds[clipName]);
		}
	}

	public void PlayAtPosition(string clipName, Vector3 pos, string soundPlayName)
	{
		if (checkForClipLoaded(clipName))
		{
			AudioSource a = getIdleAudioSource();
			soundsByName.Add(soundPlayName, a);
			a.transform.position = pos;
			a.clip = sounds[clipName];
			a.loop = true;
			a.Play();
		}
	}

	public void StopSound(string soundPlayName) {
		soundsByName[soundPlayName].Stop();
		soundsByName.Remove(soundPlayName);
	}

	AudioSource getIdleAudioSource()
	{
		foreach (AudioSource source in sources)
		{
			if (!source.isPlaying)
			{
				return source;
			}
		}
		GameObject sourceGo = new GameObject("source" + sources.Count.ToString());
		AudioSource s = sourceGo.AddComponent<AudioSource>();
		sources.Add(s);
		return s;
	}

	bool checkForClipLoaded(string clipName)
	{
		if (isClipLoaded(clipName))
		{
			return true;
		}
		else
		{
			loadClip(clipName);
			return true;
		}
	}

	void loadClip(string clipName)
	{
		AudioClip c = Resources.Load(clipName) as AudioClip;
		sounds.Add(c.name, c);
	}

	private bool isClipLoaded(string clipName)
	{
		if (sounds.ContainsKey(clipName))
		{
			return true;
		}
		return false;
	}

	void Start()
	{
		i = this;
	}
}
