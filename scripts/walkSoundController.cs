using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class walkSoundController : MonoBehaviour
{
	Vector3 lastPos;
	System.Random rnd;
	public float stepDistance = 0.8f;
	float stepDistanceAggregate = 0;
	public string walkType = "walk";
	void Start()
	{
		rnd = new System.Random();
		lastPos = transform.position;
	}

	void Update()
	{
		stepDistanceAggregate += Vector3.Distance(transform.position, lastPos);
		lastPos = transform.position;
		if (stepDistanceAggregate >= stepDistance)
		{
			int rndi = rnd.Next(0, 4);
			AudioManager.i.PlayOnce(walkType+rndi.ToString());
			stepDistanceAggregate = 0;
		}
	}
}
