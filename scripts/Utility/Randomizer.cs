using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Randomizer
{
    public static int getRandomInt(int min, int max){
		System.Random rnd = new System.Random();
		return rnd.Next(min, max);  // creates a number between 1 and 12
	}
}
