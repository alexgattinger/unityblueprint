using System;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class Item
{
    public string itemName;
    public int quantity;

    // Constructor for easy initialization
    public Item(string newName, int newQuantity)
    {
        itemName = newName;
        quantity = newQuantity;
    }
}
