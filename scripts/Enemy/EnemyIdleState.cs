using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyIdleState : State
{
    Enemy enemy;
    Player player;


    
    public EnemyIdleState(Enemy enemy, Player player)
    {
        this.enemy = enemy;
        this.player = player;
    }

    public override void Enter()
    {
		Timer.i.StartTimerEvent("findNewPoi", 5f).AddListener(enemy.findNewPoi);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
