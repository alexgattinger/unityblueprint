using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class Enemy : MonoBehaviour
{
	StateMachine sm = new StateMachine();
	NavMeshAgent na;
	EnemyIdleState idleState;

	Transform currentPoi;


	void Start()
	{
		idleState = new EnemyIdleState(this, statix.i.player);
		na = GetComponent<NavMeshAgent>();
		sm.ChangeState(idleState);
	}

	public void findNewPoi()
	{
		currentPoi = statix.i.pois[Randomizer.getRandomInt(0, statix.i.pois.Count)];
	}

	public void test()
	{
		
	}

	void Update()
	{
		sm.Run();
	}
}
