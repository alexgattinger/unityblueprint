using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Timer : MonoBehaviour
{
	public static Timer i;

	Dictionary<string, float> timers = new Dictionary<string, float>();
	Dictionary<string, UnityEvent> events = new Dictionary<string, UnityEvent>();
	// Start is called before the first frame update
	void Start()
	{
		i = this;
	}

	public void StartTimer(string name, float time)
	{
		timers.Add(name, time);
	}

	public UnityEvent StartTimerEvent(string name, float time)
	{
		UnityEvent ne = new UnityEvent();
		events.Add(name, ne);
		timers.Add(name, time);
		return ne;
	}

	public void DisposeTimer(string name)
	{
		timers.Remove(name);
	}

	public float GetTime(string name)
	{
		return timers[name];
	}

	public void Dispose(string name)
	{
		timers.Remove(name);
		events.Remove(name);
	}

	// Update is called once per frame
	void Update()
	{
		Dictionary<string, float> timersBuffer = new Dictionary<string, float>();
		foreach (KeyValuePair<string, float> timer in timers)
		{
			float newTime = timer.Value - Time.deltaTime;
			if (timers[timer.Key] <= 0)
			{
				if (events.ContainsKey(timer.Key)) //wenns ein event ist
				{
					events[timer.Key].Invoke();
					events.Remove(timer.Key);
				} else { //wens kein event ist soll timer weiter rennen
					timersBuffer.Add(timer.Key, newTime);
				}
			}
			else
			{
				timersBuffer.Add(timer.Key, newTime);
			}
		}
		timers = timersBuffer;
	}
}
